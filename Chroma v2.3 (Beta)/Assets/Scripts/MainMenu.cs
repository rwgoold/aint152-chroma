﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	private int SceneNumber = 0;

	public void Quit(){
		Application.Quit ();
	}

	public void ChangeScene(int Level){
		SceneManager.LoadScene (Level);
	}
}
