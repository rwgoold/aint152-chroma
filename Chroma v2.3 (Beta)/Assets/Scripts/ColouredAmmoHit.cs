﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColouredAmmoHit : MonoBehaviour {

	public int damage = 5 ;
	public int noDamage = 0;
	public string damageTag = "" ;
	public string wallsTag = "Walls";

	void OnTriggerEnter2D( Collider2D other ){
		if (other.CompareTag (damageTag)) {
			other.SendMessage ("TakeDamage", damage);
		} else if (other.CompareTag (wallsTag)) {
			other.SendMessage ("TakeDamage", noDamage);
		}
		Destroy( gameObject );
	}
}
