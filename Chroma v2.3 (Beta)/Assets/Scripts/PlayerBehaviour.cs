﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour {


	public delegate void UpdateHealth (int newHealth);
	public static event UpdateHealth OnUpdateHealth;


	public int charHealth = 100;
	private Animator weaponAnim;
	public int damage = 5;


	void Start() {
		weaponAnim = GetComponent< Animator> ();

		SendHealthData ();
	}

	void Update() {
		if (charHealth <= 0){
			Die ();
		}
	}

	public void OnCollisionEnter2D(Collision2D enemy){
		if (enemy.gameObject.tag == "RedEnemy" || enemy.gameObject.tag == "GreenEnemy" || enemy.gameObject.tag == "BlueEnemy" || enemy.gameObject.tag == "PurpleEnemy" || enemy.gameObject.tag =="OrangeEnemy" || enemy.gameObject.tag == "YellowEnemy") {
			TakeDamage ();
		}
	}




	public void TakeDamage (){
		charHealth -= damage;

		SendHealthData ();
	}

	public void TakesDamage(int damage){
		charHealth -= damage;

		SendHealthData ();


	}

	void Die()
	{
		SceneManager.LoadScene (0);

	}

	void SendHealthData()
	{
		if (OnUpdateHealth != null)
		{
			OnUpdateHealth (charHealth);
		}
	}
}