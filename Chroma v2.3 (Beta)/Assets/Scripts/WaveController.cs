﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveController : MonoBehaviour {

	public float redTurnOn = 5f;
	public float greenTurnOn = 15f;
	public float blueTurnOn = 30f;
	public float purpleTurnOn = 45f;
	public float orangeTurnOn = 60f;
	public float yellowTurnOn = 75f;


	public int redEnemyCount = 0;
	public int greenEnemyCount = 0;
	public int blueEnemyCount = 0;
	public int purpleEnemyCount = 0;
	public int orangeEnemyCount = 0;
	public int yellowEnemyCount = 0;

	public GameObject redEnemyPrefab;
	public GameObject greenEnemyPrefab;
	public GameObject blueEnemyPrefab;
	public GameObject purpleEnemyPrefab;
	public GameObject orangeEnemyPrefab;
	public GameObject yellowEnemyPrefab;

	void Start() {
		RedSpawn ();
	}


	void Update () {
		redTurnOn -= Time.deltaTime;
		greenTurnOn -= Time.deltaTime;
		blueTurnOn -= Time.deltaTime;
		purpleTurnOn -= Time.deltaTime;
		orangeTurnOn -= Time.deltaTime;
		yellowTurnOn -= Time.deltaTime;

		if (redTurnOn <= 0f) {
			if (redEnemyCount <= 0) {
				RedSpawn ();
			}
		}

		if (greenTurnOn <= 0f) {
			if (greenEnemyCount <= 0) {
				GreenSpawn ();
			}
		}

		if (blueTurnOn <= 0f) {
			if (blueEnemyCount <= 0) {
				BlueSpawn ();
			}
		}

		if (purpleTurnOn <= 0f) {
			if (purpleEnemyCount <= 0) {
				PurpleSpawn ();
			}
		}

		if (orangeTurnOn <= 0f) {
			if (orangeEnemyCount <= 0) {
				OrangeSpawn ();
			}
		}

		if (yellowTurnOn <= 0f) {
			if (yellowEnemyCount <= 0) {
				YellowSpawn ();
			}
		}

		TimerReset ();
	}


	public void RedSpawn(){		
		for (int i = 0; i < 1; i++){	
		Instantiate (redEnemyPrefab, transform.position, transform.rotation);
		redEnemyCount++;
		}
	}


	public void GreenSpawn(){
		for (int i = 0; i < 1; i++){	
			Instantiate (greenEnemyPrefab, transform.position, transform.rotation);
			greenEnemyCount++;
		}
	}


	public void BlueSpawn(){
		for (int i = 0; i < 1; i++){	
			Instantiate (blueEnemyPrefab, transform.position, transform.rotation);
			blueEnemyCount++;
		}
	}


	public void PurpleSpawn(){
		for (int i = 0; i < 1; i++){	
			Instantiate (purpleEnemyPrefab, transform.position, transform.rotation);
			purpleEnemyCount++;
		}
	}


	public void OrangeSpawn(){
		for (int i = 0; i < 1; i++){	
			Instantiate (orangeEnemyPrefab, transform.position, transform.rotation);
			orangeEnemyCount++;
		}
	}


	public void YellowSpawn(){
		for (int i = 0; i < 1; i++){	
			Instantiate (yellowEnemyPrefab, transform.position, transform.rotation);
			yellowEnemyCount++;
		}
	}

	public void TimerReset(){
		if ((redEnemyCount == 0) && (greenEnemyCount == 0) && (blueEnemyCount == 0) && (purpleEnemyCount == 0) && (orangeEnemyCount == 0) && (yellowEnemyCount == 0)){
			redTurnOn = 5f;
			greenTurnOn = 20f;
			blueTurnOn = 35f;
			purpleTurnOn = 50f;
			orangeTurnOn = 75f;
			yellowTurnOn = 90f;
		}
	}	
}