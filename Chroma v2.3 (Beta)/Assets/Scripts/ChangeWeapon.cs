﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeWeapon : MonoBehaviour {

	public GameObject[] weaponPrefabs = new GameObject[7];
	public GameObject[] ammoPrefabs = new GameObject[7];

	public Transform weaponSpawn;
	public Transform ammoSpawn;

	public string weaponName;
	public string ammoName;

	public float numWep = 1.0f;
	public float pos = 0.0f;

	public GameObject selectedWeapon;
	public GameObject selectedAmmo;

	public int normalAmmoAmount = 99;
	public int redAmmoAmount = 10;
	public int greenAmmoAmount = 5;
	public int blueAmmoAmount = 10;
	public int purpleAmmoAmount = 10;
	public int orangeAmmoAmount = 10;
	public int yellowAmmoAmount = 10;


	public GameObject SelectedWeapon {
		get {
			return selectedWeapon;
		}
		set {
			selectedWeapon = value;
		}
	}

	public GameObject SelectedAmmo {
		get {
			return selectedAmmo;
		}
		set {
			selectedAmmo = value;
		}
	}


	public void SwitchWeapon(string wp){
		switch (wp){
		case "weapon_norm_idle_and_anim_0": // normal weapon
			for (int i = 0; i < weaponPrefabs.Length; i++) {
				if (i != 0)
					weaponPrefabs [i].SetActive (false);
				else {
					weaponPrefabs [i].SetActive (true);
					selectedWeapon = weaponPrefabs [i];
					numWep = 1.0f;
				}
			}
			break;

		case "weapon_coloured_idle_0": // red weapon
			for (int i = 0; i < weaponPrefabs.Length; i++) {
				if (i != 1)
					weaponPrefabs [i].SetActive (false);
				else {
					weaponPrefabs [i].SetActive (true);
					selectedWeapon = weaponPrefabs [i];
					numWep = 2.0f;

				}
			}
			break;

		case "weapon_coloured_idle_1": // green weapon
			for (int i = 0; i < weaponPrefabs.Length; i++){
				if (i != 2)
					weaponPrefabs [i].SetActive (false);
				else {
					weaponPrefabs [i].SetActive (true);
					selectedWeapon = weaponPrefabs [i];
					numWep = 3.0f;
				}
			}
			break;

		case "weapon_coloured_idle_2": // blue weapon
			for (int i = 0; i < weaponPrefabs.Length; i++){
				if (i != 3)
					weaponPrefabs [i].SetActive (false);
				else
					weaponPrefabs [i].SetActive (true);
				selectedWeapon = weaponPrefabs [i];
				numWep = 4.0f;
			}
			break;

		case "weapon_coloured_idle_3": // purple weapon
			for (int i = 0; i < weaponPrefabs.Length; i++){
				if (i != 4)
					weaponPrefabs [i].SetActive (false);
				else {
					weaponPrefabs [i].SetActive (true);
					selectedWeapon = weaponPrefabs [i];
					numWep = 5.0f;
				}
			}
			break;

		case "weapon_coloured_idle_4": // orange weapon
			for (int i = 0; i < weaponPrefabs.Length; i++){
				if (i != 5)
					weaponPrefabs [i].SetActive (false);
				else {
					weaponPrefabs [i].SetActive (true);
					selectedWeapon = weaponPrefabs [i];
					numWep = 6.0f;
				}
			}
			break;

		case "weapon_coloured_idle_5": // yellow  weapon
			for (int i = 0; i < weaponPrefabs.Length; i++){
				if (i != 6)
					weaponPrefabs [i].SetActive (false);
				else {
					weaponPrefabs [i].SetActive (true);
					selectedWeapon = weaponPrefabs [i];
					numWep = 7.0f;
				}
			}
			break;
		}
	}


	public void SwitchAmmo(string ap){
		switch (ap) {
		case "weapon_ammo_norm": // normal ammo
			for (int i = 0; i < ammoPrefabs.Length; i++) {
				if (i != 0)
					ammoPrefabs [i].SetActive (false);
				else {
					ammoPrefabs [i].SetActive (true);
					selectedAmmo = ammoPrefabs [i];

				}
			}
			break;

		case "weapon_ammo_red": // red ammo
			for (int i = 0; i < ammoPrefabs.Length; i++) {
				if (i != 1)
					ammoPrefabs [i].SetActive (false);
				else { 
					ammoPrefabs [i].SetActive (true);
					selectedAmmo = ammoPrefabs [i];
				}
			}
			break;

		case "weapon_ammo_green": // green ammo
			for (int i = 0; i < ammoPrefabs.Length; i++) {
				if (i != 2)
					ammoPrefabs [i].SetActive (false);
				else {
					ammoPrefabs [i].SetActive (true);
					selectedAmmo = ammoPrefabs [i];
				}
			}
			break;

		case "weapon_ammo_blue": // blue ammo
			for (int i = 0; i < ammoPrefabs.Length; i++) {
				if (i != 3)
					ammoPrefabs [i].SetActive (false);
				else {
					ammoPrefabs [i].SetActive (true);
					selectedAmmo = ammoPrefabs [i];
				}
			}
			break;

		case "weapon_ammo_purple": // purple ammo
			for (int i = 0; i < ammoPrefabs.Length; i++) {
				if (i != 4)
					ammoPrefabs [i].SetActive (false);
				else {
					ammoPrefabs [i].SetActive (true);
					selectedAmmo = ammoPrefabs [i];
				}
			}
			break;

		case "weapon_ammo_orange": // orange ammo
			for (int i = 0; i < ammoPrefabs.Length; i++) {
				if (i != 5)
					ammoPrefabs [i].SetActive (false);
				else {
					ammoPrefabs [i].SetActive (true);
					selectedAmmo = ammoPrefabs [i];
			}
		}
			break;

		case "weapon_ammo_yellow": // yellow  ammo
			for (int i = 0; i < ammoPrefabs.Length; i++) {
				if (i != 6)
					ammoPrefabs [i].SetActive (false);
				else {
					ammoPrefabs [i].SetActive (true);
					selectedAmmo = ammoPrefabs [i];
			}
		}
			break;
		}
	}

	void Start(){
		SwitchWeapon ("weapon_norm_idle_and_anim_0");
		SwitchAmmo ("weapon_ammo_norm");
	}



	void Update () {
		if (Input.GetKeyDown (KeyCode.Alpha1) || (numWep == 1.0f)) { // normal weapon and ammo
			SwitchWeapon ("weapon_norm_idle_and_anim_0");
			SwitchAmmo ("weapon_ammo_norm");
		} else if (Input.GetKeyDown (KeyCode.Alpha2) || (numWep == 2.0f)) { // red weapon and ammo
			SwitchWeapon ("weapon_coloured_idle_0");
			SwitchAmmo ("weapon_ammo_red");
		} else if (Input.GetKeyDown (KeyCode.Alpha3) || (numWep == 3.0f)) { // green weapon and ammo
			SwitchWeapon ("weapon_coloured_idle_1");
			SwitchAmmo ("weapon_ammo_green");
		} else if (Input.GetKeyDown (KeyCode.Alpha4) || (numWep == 4.0f)) { // blue weapon and ammo
			SwitchWeapon ("weapon_coloured_idle_2");
			SwitchAmmo ("weapon_ammo_blue");
		} else if (Input.GetKeyDown (KeyCode.Alpha5) || (numWep == 5.0f)) { // purple weapon and ammo
			SwitchWeapon ("weapon_coloured_idle_3");
			SwitchAmmo ("weapon_ammo_purple");
		} else if (Input.GetKeyDown (KeyCode.Alpha6) || (numWep == 6.0f)) { // orange weapon and ammo
			SwitchWeapon ("weapon_coloured_idle_4");
			SwitchAmmo ("weapon_ammo_orange");
		} else if (Input.GetKeyDown (KeyCode.Alpha7) || (numWep == 7.0f)) { // yellow weapon and ammo
			SwitchWeapon ("weapon_coloured_idle_5");
			SwitchAmmo ("weapon_ammo_yellow");
		}

		pos = Input.GetAxis("Mouse ScrollWheel");
		if (pos > 0.0f) {
			numWep++;
			if (numWep > 7.0f){
				numWep = 1.0f;
			}
		} else if (pos < 0.0f) {
			numWep--;
			if (numWep < 1.0f){
				numWep = 7.0f;
			}
		}
	}
}

// go through if statement again