﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoHit2D : MonoBehaviour {

	public int damage = 2 ;
	public string damageTagRed = "RedEnemy" ;
	public string damageTagGreen = "GreenEnemy" ;
	public string damageTagBlue = "BlueEnemy" ;
	public string damageTagPurple= "PurpleEnemy" ;
	public string damageTagOrange = "OrangeEnemy" ;
	public string damageTagYellow = "YellowEnemy" ;

	void OnTriggerEnter2D( Collider2D other ){		
		if( other.CompareTag( damageTagRed) || other.CompareTag(damageTagGreen) || other.CompareTag(damageTagBlue) || other.CompareTag(damageTagPurple) || other.CompareTag(damageTagOrange) || other.CompareTag(damageTagYellow) ) {

			other.SendMessage( "TakeDamage" , damage );
		}
		Destroy( gameObject );
	}
}
