﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCharacteristics : MonoBehaviour {

	public int health = 1000 ;

	public void TakeDamage( int damage ){

		if( health <= 0 ) {

			Destroy( gameObject );
		}
	}

}
