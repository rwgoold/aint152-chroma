﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoAndShooting : MonoBehaviour {

	public float destroyTime = 10f;
	public float ammoSpeed = 5f;
	public float fireTime = 0.5f;

	public float normalDamage = 1f;
	public float diffColourDamage = 2f;
	public float sameColourDamage = 0f;

	public GameObject ammoPrefab;
	public Transform ammoSpawn;

	private ChangeWeapon chosenWeapon;
	private bool isFiring = false ;

	public void Start(){
		chosenWeapon = gameObject.GetComponent<ChangeWeapon> ();
	}
		

	public GameObject SelAmmo {
		get {
			return ammoPrefab;
		}
		set {
			ammoPrefab = value;
		}
	}
		

	void SetFiring(){
		isFiring = false ;
	}


	void Fire(){
		isFiring = true ;
		Instantiate( ammoPrefab, ammoSpawn.position, ammoSpawn.rotation );

		if( GetComponent< AudioSource >() != null ) {
			GetComponent< AudioSource >().Play();
		}
		Invoke( "SetFiring" , fireTime );
		UpdateAmmoUse ();
	}


	void CheckAmmo(){
		if (chosenWeapon.numWep == 1.0f) {
			if (chosenWeapon.normalAmmoAmount > 0) {
				Fire ();
			} else
				chosenWeapon.normalAmmoAmount = 0;
				Debug.Log ("No Normal Ammo");
		}

		if (chosenWeapon.numWep == 2.0f) {
			if (chosenWeapon.redAmmoAmount > 0) {
				Fire ();
			} else
				chosenWeapon.redAmmoAmount = 0;
				Debug.Log ("No Red Ammo");
		}

		if (chosenWeapon.numWep == 3.0f) {
			if (chosenWeapon.greenAmmoAmount > 0) {
				Fire ();
			} else
				chosenWeapon.greenAmmoAmount = 0;
				Debug.Log ("No Green Ammo");
		}

		if (chosenWeapon.numWep == 4.0f) {
			if (chosenWeapon.blueAmmoAmount > 0) {
				Fire ();
			} else
				chosenWeapon.blueAmmoAmount = 0;
				Debug.Log ("No Blue Ammo");
		}

		if (chosenWeapon.numWep == 5.0f) {
			if (chosenWeapon.purpleAmmoAmount > 0) {
				Fire ();
			} else
				chosenWeapon.purpleAmmoAmount = 0;
				Debug.Log ("No Purple Ammo");
		}

		if (chosenWeapon.numWep == 6.0f) {
			if (chosenWeapon.orangeAmmoAmount > 0) {
				Fire ();
			} else
				chosenWeapon.orangeAmmoAmount = 0;
				Debug.Log ("No Orange Ammo");
		}

		if (chosenWeapon.numWep == 7.0f) {
			if (chosenWeapon.yellowAmmoAmount > 0) {
				Fire ();
			} else
				chosenWeapon.yellowAmmoAmount = 0;
				Debug.Log ("No Yellow Ammo");
		}
	}


	void UpdateAmmoUse(){
		if (chosenWeapon.numWep == 1.0f) {
			chosenWeapon.normalAmmoAmount -= 1;
		}

		if (chosenWeapon.numWep == 2.0f) {
			chosenWeapon.redAmmoAmount -= 1;
		}

		if (chosenWeapon.numWep == 3.0f) {
			chosenWeapon.greenAmmoAmount -= 1;
		}

		if (chosenWeapon.numWep == 4.0f) {
			chosenWeapon.blueAmmoAmount -= 1;
		}

		if (chosenWeapon.numWep == 5.0f) {
			chosenWeapon.purpleAmmoAmount -= 1;
		}

		if (chosenWeapon.numWep == 6.0f) {
			chosenWeapon.orangeAmmoAmount -= 1;
		}

		if (chosenWeapon.numWep == 7.0f) {
			chosenWeapon.yellowAmmoAmount -= 1;
		}
	}




	void Update(){
		if( Input .GetMouseButton( 0 ) ) {
			if( !isFiring ) {
				ammoPrefab = chosenWeapon.SelectedAmmo;
				CheckAmmo();
					}
				}
			}
					

	// Set and Get Methods
	public float AmmoSpeed {
		get {
			return ammoSpeed;
		}
		set {
			ammoSpeed = value;
		}
	}

	public float FireTime {
		get {
			return fireTime;
		}
		set {
			fireTime = value;
		}
	}


	public float NormDam {
		get {
			return normalDamage;
		}
		set {
			normalDamage = value;
		}
	}

	public float DiffColDam {
		get {
			return diffColourDamage;
		}
		set {
			diffColourDamage = value;
		}
	}

	public float SameColDam {
		get {
			return diffColourDamage;
		}
		set {
			diffColourDamage = value;
		}
	}
}


