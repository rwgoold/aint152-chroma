﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharInventory : MonoBehaviour 
{
	// Class of the ammo types
	public class StoredAmmo 
	{
		public int normAmmo;
		public int redAmmo;
		public int greenAmmo;
		public int blueAmmo;
		public int purpleAmmo;
		public int orangeAmmo;
		public int yellowAmmo;
	

	// 
		public StoredAmmo(int norm, int red, int grn, int blu, int pur, int org, int yel)
		{
			normAmmo = norm;
			redAmmo = red;
			greenAmmo = grn;
			purpleAmmo = pur;
			orangeAmmo = org;
			yellowAmmo = yel;
		}


	// Constructor
		public StoredAmmo()
		{
			normAmmo = 50;
			redAmmo = 50;
			greenAmmo = 50;
			blueAmmo = 50;
			purpleAmmo = 50;
			orangeAmmo = 50;
			yellowAmmo = 50;
		}
	}



	// Creating an Instance (an Object) of the StoredAmmo class
	public StoredAmmo myAmmo = new StoredAmmo(50, 10, 10, 10, 10, 10, 10);


	// Use this for initialization
	void Start () {
		

}
}
