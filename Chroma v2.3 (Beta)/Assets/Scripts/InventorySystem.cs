﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySystem : MonoBehaviour {

	public int normalAmount = 1;
	public int redAmount = 0;
	public int greenAmount = 0;
	public int blueAmount = 0;
	public int purpleAmount = 0;
	public int orangeAmount = 0;
	public int yellowAmount = 0;

}

