﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour {

	public GameObject enemyPrefab;

	public void Spawn(){
		Instantiate (enemyPrefab, transform.position, transform.rotation);
	}
}