﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour {

	public int health = 100;
	public int score = 0;
	private int numberOfEnemies;
	private int waveNumber;

	public Text normal;
	public Text red;
	public Text green;
	public Text blue;
	public Text purple;
	public Text orange;
	public Text yellow;

	public Text healthAmount;
	public Text scoreAmount;

	private ChangeWeapon ammo;


	void Start(){
		ammo = GameObject.FindGameObjectWithTag ("Player").GetComponent<ChangeWeapon>();
		UpdateUI ();
	}

	void OnEnable() {
		PlayerBehaviour.OnUpdateHealth += HandleonUpdateHealth;
		AddScore.OnSendScore += HandleonSendScore;
	}
		
	void Update() {
		
		normal.text = ammo.normalAmmoAmount.ToString ();
		red.text = ammo.redAmmoAmount.ToString ();
		green.text = ammo.greenAmmoAmount.ToString ();
		blue.text = ammo.blueAmmoAmount.ToString ();
		purple.text = ammo.purpleAmmoAmount.ToString ();
		orange.text = ammo.orangeAmmoAmount.ToString ();
		yellow.text = ammo.yellowAmmoAmount.ToString ();
	
	}



	void HandleonUpdateHealth( int newHealth) {
		health = newHealth;
		UpdateUI ();
	}

	void HandleonSendScore ( int theScore) {
		score += theScore;
		UpdateUI ();
	}


	void UpdateUI(){
		healthAmount.text = "Health: " + health.ToString ();
		scoreAmount.text = "Score: " + score.ToString ();
	}		
}
